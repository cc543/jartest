import com.barclays.DB;
import com.barclays.Restaurant;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        new DB("jdbc:sqlite::memory:");
        new Restaurant("A1 Foodz", "A1Foodz.URL");
        Restaurant.init();
        DB.conn.close();
    }
}
